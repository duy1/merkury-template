import '../styles/style.less';
import headerModule from './modules/header';
import pageHomeModule from './pages/home';
import application from './core/application';

application = require('./core/application');

const applicationModules = [
  // modules
  { name: 'moduleHeader', content: headerModule },

  // pages
  { name: 'pageHome', content: pageHomeModule, isActive: $('.page-home').length > 0 },
];

applicationModules.forEach(({
  name,
  content,
  isActive,
}) => {
  if (isActive) {
    application.register(name, content);
  }
});


$(() => {
  application.start();
});
